﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using System;

public class AdmobNetwork : Singleton<AdmobNetwork> {

	private BannerView _banner = null;
	private InterstitialAd _interstitial = null;
	private bool isInterstitialLoading = false;
	private bool isShowInterstitialAfterLoading = false;

	protected AdmobNetwork() {
	}

	private AdRequest CreateRequest()
	{
		return new AdRequest.Builder ()
//			.AddTestDevice(AdRequest.TestDeviceSimulator)       // Simulator.
			.Build ();
	}

	private void LoadBanner()
	{
		if (_banner == null) {
			if (InhouseConstant.ADMOB_BANNER_ID == null) {
				Debug.Log ("Config of ADMOB BANNER is missed");
				return;
			}

			_banner = new BannerView (InhouseConstant.ADMOB_BANNER_ID, AdSize.SmartBanner, AdPosition.Bottom);
			_banner.LoadAd (CreateRequest ());
			Debug.Log ("Request Admob Banner");
		}
	}

	private void LoadInterstital()
	{
		if (InhouseConstant.ADMOB_INTERSITITIAL_ID == null) {
			Debug.Log ("Config of ADMOB_INTERSITITIAL_ID is missed");
			return;
		}	

		if (isInterstitialLoading == true) {
			return;
		}

		isInterstitialLoading = true;
		_interstitial = new InterstitialAd (InhouseConstant.ADMOB_INTERSITITIAL_ID);
		_interstitial.OnAdLoaded += HandleOnAdLoaded;
		_interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
		_interstitial.OnAdClosed += HandleOnAdClosed;
		_interstitial.LoadAd (CreateRequest ());
		Debug.Log ("Request Admob Interstitial");

	}

	//Handle Event Interstitial
	public void HandleOnAdLoaded( object sender, EventArgs args)
	{
		isInterstitialLoading = false;
		if (isShowInterstitialAfterLoading) {
			isShowInterstitialAfterLoading = false;
			_interstitial.Show ();
		}
	}

	public void HandleOnAdFailedToLoad( object sender, AdFailedToLoadEventArgs args)
	{
		isInterstitialLoading = false;
		Debug.Log (args.Message);
	}

	public void HandleOnAdClosed( object sender, EventArgs args)
	{
		_interstitial = null;
		LoadInterstital ();
	}


	public void ShowBanner()
	{
		if (_banner == null)
			this.LoadBanner ();
		
		_banner.Show ();
	}

	public void HideBanner()
	{
		if ( _banner != null )
			_banner.Hide ();
	}

	public void ShowInterstitial()
	{
		if (_interstitial == null) {
			isShowInterstitialAfterLoading = true;
			LoadInterstital ();
		} else {

			if (_interstitial.IsLoaded ()) {		
				_interstitial.Show ();
				Debug.Log ("Show Interstitial Admob");
			} else {
				Debug.Log ("Interstitial Admob Loading is not complete");
			}
		}
	}

}
