﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;

public class IOSGameCenter : Singleton<IOSGameCenter> {
	
	public const string _BESTSCORE = "BEST_SCORE";

	protected IOSGameCenter() {}

	public void Init()
	{
		Social.localUser.Authenticate (delegate(bool success) {
			if ( success ) {
				
				Debug.Log("GamecenterInit: PASS");
				Social.LoadScores(InhouseConstant.IOS_HIGHSCORE_ID, delegate(IScore[] scores) {
					if (scores.Length > 0) {
						
						int localBestScore = PlayerPrefs.GetInt (_BESTSCORE);
						int gcBestScore = 0;

						foreach (IScore score in scores)
						{
							
							if ( score.userID == Social.localUser.id ) {
								gcBestScore = (int)score.value;
								break;
							}
						}							

						if (localBestScore > gcBestScore) {
							Debug.Log("GC BEST SCORE: " + gcBestScore);
							Debug.Log("LOCAL BEST SCORE: " + localBestScore);
							Debug.Log("SUBMIT BEST SCORE TO GC");
							SubmitScore(localBestScore);
						}
						else if (gcBestScore > localBestScore) {
							Debug.Log("GC BEST SCORE: " + gcBestScore);
							Debug.Log("LOCAL BEST SCORE: " + localBestScore);
							Debug.Log("APPLY BEST SCORE TO LOCAL");
							SaveBestScore(gcBestScore);
						}

					}
					else
						Debug.Log ("No scores loaded");					
				});
			}else {
				Debug.Log("GamecenterInit: FAILED");
			}
		});
	}
				
	public void ShowLeaderBoard(){

		if (Social.localUser.authenticated) {
			Social.ShowLeaderboardUI ();
		}
	}

	public void SubmitScore(long score){
		if (Social.localUser.authenticated) {
			Social.ReportScore (score, InhouseConstant.IOS_HIGHSCORE_ID, HighScoreCheck);
		}

	}

	void HighScoreCheck( bool success){
		if (success) {
			Debug.Log ("SubmitScore: PASS");
		} else {
			Debug.Log ("SubmitScore: FAILED");
		}
	}

	public void SaveBestScore(int score){
		PlayerPrefs.SetInt(_BESTSCORE, score);
		PlayerPrefs.Save();
	}

}
