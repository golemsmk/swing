﻿using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections;

public class IOSPurchaseUnityService : Singleton<IOSPurchaseUnityService>, IStoreListener{

	private static IStoreController m_StoreController;          // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	protected IOSPurchaseUnityService() {
	}
		
	public void Init()
	{
		if (m_StoreController == null) {
			InitializePurchasing ();	
		}
	}

	private bool IsInitialized(){
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void InitializePurchasing(){
		
		if (IsInitialized ())
			return;

		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());
		builder.AddProduct (InhouseConstant.IOS_NOAD_ID, ProductType.NonConsumable);
		UnityPurchasing.Initialize (this, builder);
	}

	public void BuyRemoveAds(){
		
		InhouseManager.Instance.BlurPanel.SetActive (true);
		BuyProductID (InhouseConstant.IOS_NOAD_ID);

	}

	void BuyProductID(string productID){
		
		if (IsInitialized ()) {
			
			Product product = m_StoreController.products.WithID (productID);

			if (product != null && product.availableToPurchase) {

				m_StoreController.InitiatePurchase (product);

			} else {
				Debug.Log ("BuyProductID: FAIL. Not Purchasing Product, either is not found or is not available for purchase");
			}

		} else {
			Debug.Log ("BuyProductID FAIL: Not Initialize UnityPurchasing");
		}
	}
		
	public void RestorePurchases(){

		if (!IsInitialized ()) {
			Debug.Log ("Restore Purchases FAIL. Not Initialized");
		}

		InhouseManager.Instance.BlurPanel.SetActive (true);

		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
			apple.RestoreTransactions ((result) => {
				Debug.Log(" RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				InhouseManager.Instance.DoRemoveAds();
				InhouseManager.Instance.BlurPanel.SetActive (false);
			});
		} else {
			Debug.Log ("RestorePurchases FAIL. Not Support on this plaftform " + Application.platform);
		}
			
	}

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions ){
		Debug.Log ("IAP OnInitialized: PASS");
		m_StoreController = controller;
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed(InitializationFailureReason error){
		Debug.Log ("OnInitializeFailed With Reason : " + error.ToString());
	}

	public PurchaseProcessingResult ProcessPurchase( PurchaseEventArgs args ){
		if (string.Equals (args.purchasedProduct.definition.id, InhouseConstant.IOS_NOAD_ID, System.StringComparison.Ordinal)) {
			InhouseManager.Instance.DoRemoveAds();
			Debug.Log (string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
		} else {
			Debug.Log (string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}

		InhouseManager.Instance.BlurPanel.SetActive (false);
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason){
		Debug.Log (string.Format("OnPurchaseFailed: FAIL. Product '{0}' . PurchaseFailureReason: {1}", product.definition.id, failureReason.ToString()));
		InhouseManager.Instance.BlurPanel.SetActive (false);
	}
}
