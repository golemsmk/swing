﻿using UnityEngine;
using System.Collections;

public static class InhouseConstant
{
	public static string IOS_APP_ID = "1118345327";
	public static string APP_NAME = "Gravity Switch";
	public static string FEEDBACK_FORM_URL = "http://goo.gl/forms/OpkchGrRUxEXrit02";
	public static string DEVELOPER_URL = "https://itunes.apple.com/us/developer/koby-benham/id1041504025?mt=8"; //Only using it for opening moregame
	public static string IOS_NOAD_ID = "com.kobybenham.ColorBallGravitySwitch.removeAds";
	public static string IOS_HIGHSCORE_ID = "com.kobybenham.ColorBallGravitySwitch.highscore";

	//// CONFIG ADS NETWORKS
	/// 
	//Thu tu xuat hien quang cao mediation, muon thay doi chi can thay doi value
	public const int ADMOB = 1;
	public const int CHARBOOST = 3;
	public const int UNITY = 2;
	// ENDED

	public static string ADMOB_BANNER_ID = "ca-app-pub-3287739054829120/6609182495";
	public static string ADMOB_INTERSITITIAL_ID = "ca-app-pub-3287739054829120/8085915697";

	#if !UNITY_ADS
	public static string UNITY_ID = "115824";
	public static bool UNITY_TEST_MODE = true;
	#endif

	// CHARTBOOST
	// neu su dung chartboost thi thong so se dc cau hinh trong Setting Chartboost cua UnityEditor.

	public static int CRITERIA_SHOW_INTERSITITIAL = 2; // Tieu chuan danh gia show intersititial

	public static int SHOW_ADMOB_INTERSITITAL_TIMES = 2; // hien thi Admob n lan; = 0 se ko hien thi
	public static int SHOW_CHARTBOOST_INTERSTITIAL_TIMES = 2; // hien thi chartboost n lan; = 0 se ko hien thi
	public static int SHOW_UNITY_ADS_TIMES = 2; // hien thi Unity n lan ; = 0 se ko hien thi
}
