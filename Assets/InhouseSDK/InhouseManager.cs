﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
//using UnityEditor.Advertisements;
using UnityEngine.Advertisements;
using ChartboostSDK;

public class InhouseManager : Singleton<InhouseManager> 
{
	public GameObject BlurPanel;
	public GameObject SettingPanel;

	private bool _isRemoveAds = false;
	private Dictionary<int, int> _dictionaryIntersititalMediation;

	protected InhouseManager() {
		_dictionaryIntersititalMediation = new Dictionary<int, int> ();
		_dictionaryIntersititalMediation.Add (InhouseConstant.ADMOB, InhouseConstant.SHOW_ADMOB_INTERSITITAL_TIMES);
		_dictionaryIntersititalMediation.Add (InhouseConstant.CHARBOOST, InhouseConstant.SHOW_CHARTBOOST_INTERSTITIAL_TIMES);
		_dictionaryIntersititalMediation.Add (InhouseConstant.UNITY, InhouseConstant.SHOW_UNITY_ADS_TIMES);
	}

	public void Init(){

		this.checkRemoveAds ();
		this.LoadVideoUnityAds ();
		IOSPurchaseUnityService.Instance.Init ();
		IOSGameCenter.Instance.Init ();
	}
	/***
	 * Check Flag
	 * */
	private bool checkRemoveAds ()
	{
		this._isRemoveAds = PlayerPrefs.GetInt ("REMOVEADS", 0) != 0;	
		return this._isRemoveAds;
	}

	public void ShowBannerAdmob()
	{
		if ( !_isRemoveAds ) 
			AdmobNetwork.Instance.ShowBanner ();
	}

	public void HideBannerAdmob()
	{
			AdmobNetwork.Instance.HideBanner ();
	}

	public void ShowIntersititialAdmob()
	{
		if (!_isRemoveAds) {
			Debug.Log ("show Intersitital Admob");
			AdmobNetwork.Instance.ShowInterstitial ();	
		}
	}	

	public void LoadVideoUnityAds(){
		if (!_isRemoveAds) {
			#if !UNITY_ADS
			if (Advertisement.isSupported) {
				Advertisement.Initialize (InhouseConstant.UNITY_ID, InhouseConstant.UNITY_TEST_MODE);
			} else {
				Debug.Log (" The platform is not supported Unity Ads");
			}
			#endif
		}
	}

	public void ShowVideoUnityAds()
	{
		if (!_isRemoveAds) {
			Debug.Log ("Show Unity Ads");
//			while (!Advertisement.isInitialized || !Advertisement.IsReady()) {
//				yield return new WaitForSeconds(0.5f);
//			}
//			Advertisement.Show ();
			if (Advertisement.isInitialized && Advertisement.IsReady()) {
				Advertisement.Show ();
			}
		}
	}

	private void ShowManyInterstitialAds(int TypeAds ) {
		if (!_isRemoveAds) {
			
			Debug.Log ("ShowManyInterstitialAds");

			int Max_Display = 0;
			do {
				Max_Display = _dictionaryIntersititalMediation [TypeAds];
				Debug.Log ("MAx Display " + Max_Display);
				if ( Max_Display <= 0 ) {
					TypeAds++;
					if (TypeAds > _dictionaryIntersititalMediation.Count)
						TypeAds = 1;
					PlayerPrefs.SetInt ("TYPE_ADS", TypeAds);
				}
			} while (Max_Display <= 0);

			int count = PlayerPrefs.GetInt ("DISPLAY_ADS_COUNT", 1);

			if (count <= Max_Display) {
				
				switch (TypeAds) {
				case InhouseConstant.ADMOB:
					ShowIntersititialAdmob ();
					break;
				case InhouseConstant.CHARBOOST:
					ShowIntersititialChartBoost ();
					break;
				case InhouseConstant.UNITY:
					ShowVideoUnityAds ();
					break;
				};

				count++;
			}

			if (count > Max_Display) {
			
				TypeAds++;

				if (TypeAds > _dictionaryIntersititalMediation.Count)
					TypeAds = 1;
			
				count = 1;
				PlayerPrefs.SetInt ("TYPE_ADS", TypeAds);
			} 

			PlayerPrefs.SetInt ("DISPLAY_ADS_COUNT", count);

			PlayerPrefs.Save ();
		}
	}

	public void ShowIntersititialMediation(string Criteria){
		
		if (!_isRemoveAds) {
			int typeAds = PlayerPrefs.GetInt ("TYPE_ADS", 1);
			int criteriaDisplay = PlayerPrefs.GetInt (Criteria, 1); 

			Debug.Log(Criteria + " " + PlayerPrefs.GetInt(Criteria, 1));
			Debug.Log("TYPE_ADS " + PlayerPrefs.GetInt("TYPE_ADS", 1));
			Debug.Log("DISPLAY_ADS_COUNT " + PlayerPrefs.GetInt("DISPLAY_ADS_COUNT", 1));


			if (criteriaDisplay >= InhouseConstant.CRITERIA_SHOW_INTERSITITIAL) {
				ShowManyInterstitialAds (typeAds);
				criteriaDisplay = 1;
			}
			else
				criteriaDisplay++;
			
			PlayerPrefs.SetInt (Criteria, criteriaDisplay);
			PlayerPrefs.Save ();
		}

	}
	/*
	 * Processing IAP
	 * Implement your code at here.
	 */
	public void BuyNoAds(){

		IOSPurchaseUnityService.Instance.BuyRemoveAds ();
//		this.BlurPanel.SetActive (true);
//
//		IOSPurchase.Instance.BuyNoAds(delegate(ISN_Result obj) {
//			if ( obj.IsSucceeded ) {
//				DoRemoveAds();
//			}else {
//				this.BlurPanel.SetActive(false);	
//				Debug.Log(obj.Error.Description);
//			}
//			this.BlurPanel.SetActive(false);	
//		});
	}
		
	public void RestoreNoAds(){
		IOSPurchaseUnityService.Instance.RestorePurchases ();
//		this.BlurPanel.SetActive (true);
//		IOSPurchase.Instance.RestoreNoAds (delegate(ISN_Result obj) {
//			if ( obj.IsSucceeded) {
//				DoRemoveAds();
//				IOSNativePopUpManager.showMessage ("Success", "Restore Complete");
//			} else {
//					this.BlurPanel.SetActive (false);
//					IOSNativePopUpManager.showMessage ("Error" + obj.Error.Code, obj.Error.Description);
//			}
//
//			this.BlurPanel.SetActive (false);
//		});

	}

	public void DoRemoveAds(){
		PlayerPrefs.SetInt ("REMOVEADS", 1);
		PlayerPrefs.Save ();
		checkRemoveAds ();
		HideBannerAdmob ();			
	}

	/*
	 * GameCenter
	 * 
	 */

	public void ShowGameCenterLeaderBoard()
	{
		IOSGameCenter.Instance.ShowLeaderBoard ();
	}

	public bool CheckBestScoreAndSaveIfNeed(int score){
		int localBestScore = PlayerPrefs.GetInt (IOSGameCenter._BESTSCORE, 0);
		if(score > localBestScore){
			IOSGameCenter.Instance.SubmitScore(score);
			IOSGameCenter.Instance.SaveBestScore(score);
			Debug.Log ("Save Score to GameCenter");
			return true;
		}
		else {
			Debug.Log ("Current Score is small");
			return false;

		}
	}

	/*
	 * Sharing, Feedback, Rate, MoreGames
	 * 
	 */

	public void RateUs(){
		string AppReviewURL = "https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id={0}&pageNumber=0&%20sortOrdering=1&type=Purple%20Software&mt=8";
		Application.OpenURL(string.Format(AppReviewURL, InhouseConstant.IOS_APP_ID));
	}

	public void Feedback(){
		Application.OpenURL (InhouseConstant.FEEDBACK_FORM_URL);
	}

	public void ShowMoreApps(){
		Application.OpenURL (InhouseConstant.DEVELOPER_URL);
	}

	public void Share(){
		int bestScore = PlayerPrefs.GetInt (IOSGameCenter._BESTSCORE, 0);
		string URL_TEMPLATE = "https://itunes.apple.com/US/app/id{0}?l=US&mt=8";
		string URL = string.Format (URL_TEMPLATE, InhouseConstant.IOS_APP_ID);
		string s = "My best score: " + bestScore + ". Get it for free: " + URL;

		NativeShare m = GetComponent<NativeShare>();

		if(m == null)
		{
			gameObject.AddComponent<NativeShare>();
		}

		m = GetComponent<NativeShare>();

		m.Share(s,"",URL,InhouseConstant.APP_NAME);
		//		UM_ShareUtility.ShareMedia("Color swipe","My best score: " + bestScore + ". Get it for free: " + "http://barouch.fr/colorswipe.php",textureForPost);		
	}

	/*
	 * ChartBoost
	 * 
	 */

	public void ShowIntersititialChartBoost(){
		if (!_isRemoveAds) {
			Debug.Log ("Show Chartboost");
			Chartboost.showInterstitial (CBLocation.GameOver);
		}
	}

	//Panel Settings
	public void OnClickedSetting(){
		Debug.Log ("open seting");
		if (SettingPanel != null)
			Debug.Log ("Co Panel setting ma");
		SettingPanel.SetActive (true);
		Debug.Log (SettingPanel.ToString ());
	}

	public void OnClickedSettingClose(){
		Debug.Log (" Close Settings");
		SettingPanel.SetActive (false);
	}

}
