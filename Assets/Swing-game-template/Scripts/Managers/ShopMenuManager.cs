using UnityEngine;
using System.Collections;

public class ShopMenuManager : MonoBehaviour {
	
	///*************************************************************************///
	/// Main Menu Controller.
	/// This class handles all touch events on buttons, and also updates the 
	/// player status on screen.
	///*************************************************************************///

	public PriceInfor[] price;

	private float buttonAnimationSpeed = 9;		//speed on animation effect when tapped on button
	private bool canTap = true;					//flag to prevent double tap
	public AudioClip tapSfx;					//tap sound for buttons click
	
	//Reference to GameObjects
	public GameObject coin;				//UI 3d text object


	//*****************************************************************************
	// Init. Updates the 3d texts with saved values fetched from playerprefs.
	//*****************************************************************************
	void Awake (){
		coin.GetComponent<TextMesh>().text = "Coin: "+PlayerPrefs.GetInt("Coin").ToString();
		for (int i = 0; i < price.Length; i++) {
			price [i].priceTag = price [i].button.transform.GetChild (1).GetComponent<TextMesh> ();
			if (PlayerPrefs.GetInt (price [i].button.name, 0) != 0) {
				price [i].priceTag.text = "Unlocked!";
				if (price [i].button.name == PlayerPrefs.GetString ("PlayerCharacter", "bulbasaur"))
					price [i].priceTag.text = "Playing!";
			} else {
				price [i].priceTag.text = price [i].price.ToString ()+" coins";
			}
		}
	}


	//*****************************************************************************
	// FSM
	//*****************************************************************************
	void Update (){	
		if(canTap) {
			StartCoroutine(tapManager());
		}
	}


	//*****************************************************************************
	// This function monitors player touches on menu buttons.
	// detects both touch and clicks and can be used with editor, handheld device and 
	// every other platforms at once.
	//*****************************************************************************
	private RaycastHit hitInfo;
	private Ray ray;
	IEnumerator tapManager (){
		
		//Mouse of touch?
		if(	Input.touches.Length > 0 && Input.touches[0].phase == TouchPhase.Ended)  
			ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
		else if(Input.GetMouseButtonUp(0))
			ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		else
			yield break;
		
		if (Physics.Raycast(ray, out hitInfo)) {
			GameObject objectHit = hitInfo.transform.gameObject;
			playSfx(tapSfx);							//play touch sound
			StartCoroutine(animateButton(objectHit));	//touch animation effect
			yield return new WaitForSeconds(0.1f);
			int indexButton = 0;
			if (objectHit.name == "bulbasaur")
				indexButton = 0;
			else if (objectHit.name == "charmander")
				indexButton = 1;
			else if (objectHit.name == "duck")
				indexButton = 2;
			else if (objectHit.name == "pigeon")
				indexButton = 3;
			else if (objectHit.name == "pikachu")
				indexButton = 4;
			else {
				Application.LoadLevel ("Menu");
				yield break;
			}
			if (PlayerPrefs.GetInt (objectHit.name, 0) != 0) {
				PlayerPrefs.SetString ("PlayerCharacter", objectHit.name);
				price[indexButton].priceTag.text = "Playing!";
				for (int i = 0; i < price.Length; i++) {
					if (PlayerPrefs.GetInt (price [i].button.name, 0) != 0) {
						price [i].priceTag.text = "Unlocked!";
						if (price [i].button.name == PlayerPrefs.GetString ("PlayerCharacter", "bulbasaur"))
							price [i].priceTag.text = "Playing!";
					} else {
						price [i].priceTag.text = price [i].price.ToString ()+" coins";
					}
				}
			}
			else if (PlayerPrefs.GetInt("Coin")>=price[indexButton].price) {
				PlayerPrefs.SetInt ("Coin", PlayerPrefs.GetInt("Coin")-price[indexButton].price);
				PlayerPrefs.SetInt (objectHit.name, 1);
				price[indexButton].priceTag.text = "Unlocked!";
				coin.GetComponent<TextMesh>().text = "Coin: "+PlayerPrefs.GetInt("Coin").ToString();
			}
			yield return new WaitForSeconds(0.9f);
		}
	}

	
	//*****************************************************************************
	// This function animates a button by modifying it's scales on x-y plane.
	// can be used on any element to simulate the tap effect.
	//*****************************************************************************
	IEnumerator animateButton ( GameObject _btn  ){
		canTap = false;
		Vector3 startingScale = _btn.transform.localScale;	//initial scale	
		Vector3 destinationScale = startingScale * 1.1f;	//target scale
		
		//Scale up
		float t = 0.0f; 
		while (t <= 1.0f) {
			t += Time.deltaTime * buttonAnimationSpeed;
			_btn.transform.localScale = new Vector3(Mathf.SmoothStep(startingScale.x, destinationScale.x, t),
			                                        _btn.transform.localScale.y,
			                                        Mathf.SmoothStep(startingScale.z, destinationScale.z, t));
			yield return 0;
		}
		
		//Scale down
		float r = 0.0f; 
		if(_btn.transform.localScale.x >= destinationScale.x) {
			while (r <= 1.0f) {
				r += Time.deltaTime * buttonAnimationSpeed;
				_btn.transform.localScale = new Vector3(Mathf.SmoothStep(destinationScale.x, startingScale.x, r),
				                                        _btn.transform.localScale.y,
				                                        Mathf.SmoothStep(destinationScale.z, startingScale.z, r));
				yield return 0;
			}
		}
		
		if(r >= 1)
			canTap = true;
	}


	//*****************************************************************************
	// Play sound clips
	//*****************************************************************************
	void playSfx ( AudioClip _clip  ){
		GetComponent<AudioSource>().clip = _clip;
		if(!GetComponent<AudioSource>().isPlaying) {
			GetComponent<AudioSource>().Play();
		}
	}
	
}
	
[System.Serializable]
public class PriceInfor
{
	public TextMesh priceTag;
	public int		price;
	public GameObject button;
}